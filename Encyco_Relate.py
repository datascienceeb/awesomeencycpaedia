import pandas as pd
import numpy as np
# import jenkspy
try:
  import xml.etree.cElementTree as et
except ImportError:
  import xml.etree.ElementTree as et

#preparation for xpath in xml
tagtext = '{http://www.loc.gov/standards/alto/v3/alto.xsd}'
path_TexLine = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine'
path_Relate_String = tagtext+'String'
path_String = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine/'+tagtext+'String'
relate_count = 0
removelist = ['I.','II.','III.','IV.','V.','VI.','VII.','VIII.', 'IX.', 'X.', 'Tit.','Plate']


for number in [144133901,144133902,144133903]: #1771 edition number
  index_csv_path = 'encyclopaediaBritannica/'+str(number)+'/'+str(number)+'-mets.xml.csv'
  index_df = pd.read_csv(index_csv_path)
  for xml_filename in index_df['FILEID']:
    xml_path = 'encyclopaediaBritannica/'+str(number)+'/alto/'+xml_filename[5:]+'.xml'
  # tree = et.parse('encyclopaediaBritannica/144133901/alto/188082943.34.xml')
    tree = et.parse(xml_path)
    print('FILEID = ', xml_filename)


    # textline_HPOS_list = []
    # for textline in tree.iterfind(path_TexLine):
    #   if int(textline.attrib['HEIGHT']) < 40 & int(textline.attrib['HPOS'])<2000:
    #     textline_HPOS_list.append(int(textline.attrib['HPOS']))
    # breaks = jenkspy.jenks_breaks(textline_HPOS_list, nb_class=4)
    # print(breaks)

    #Get left&right mother word position
    left_HPOS_list = []
    for textline in tree.iterfind(path_TexLine):
      if (int(textline.attrib['HPOS']) < 500):
        left_HPOS_list.append(int(textline.attrib['HPOS']))
    left_HPOS_list_mother = []
    for i in range(len(left_HPOS_list)-1):
      if left_HPOS_list[i+1] - left_HPOS_list[i] > 10:
        left_HPOS_list_mother.append(left_HPOS_list[i])
    # print(left_HPOS_list_mother)


    right_HPOS_list = []
    for textline in tree.iterfind(path_TexLine):
      if  (int(textline.attrib['HPOS']) < 1500) & (int(textline.attrib['HPOS']) > 1000):
        right_HPOS_list.append(int(textline.attrib['HPOS']))
    # print(right_HPOS_list)
    right_HPOS_list_mother = []
    for i in range(len(right_HPOS_list)-1):
      if right_HPOS_list[i+1] - right_HPOS_list[i] > 10:
        right_HPOS_list_mother.append(right_HPOS_list[i])
    # print(right_HPOS_list_mother)


    # iterate each line
    wordlist = []
    motherword_list = []
    word_relate_list = []
    mother_count = 0
    see_signal = False
    hyphen_signal = False
    if (left_HPOS_list_mother != []) & (right_HPOS_list_mother != []):
      for textline in tree.iterfind(path_TexLine):
        # Iterate every string in the line
        motherline = False # if this is a mother word line, then motherline = True
        for string in textline.iterfind(path_Relate_String):

          if (string.attrib['CONTENT'].isupper()) & ((int(string.attrib['HPOS']) in range(min(left_HPOS_list_mother)-1, max(left_HPOS_list_mother)+1)) | (int(string.attrib['HPOS']) in range(min(right_HPOS_list_mother)-1, max(right_HPOS_list_mother)+1))):
            # print('Mother Word - ', string.attrib['CONTENT'])
            wordlist.append(motherword_list)
            motherword_list = []
            motherword_list.append(string.attrib['CONTENT'])
            motherline = True

            continue    #Avoid printing captialize word twice
          #Find motherword wirh "or"
          # if (motherline == True) & (string.attrib['CONTENT'] == 'or'):
          #   # print('Or Signal - ', string.attrib['CONTENT'])
          #   motherword_list.append(string.attrib['CONTENT'])
          #Find indicator "See"
          if string.attrib['CONTENT'] == 'See':
            # print('See Signal - ', string.attrib['CONTENT'])
            motherword_list.append(string.attrib['CONTENT'])
            see_signal  = True
            continue
          #Find word inside the explination
          if (see_signal == True) & (string.attrib['CONTENT'][0].isupper()) & (string.attrib['CONTENT'].upper() not in removelist):
            motherword_list.append(string.attrib['CONTENT'].upper())
            see_signal = False
            continue
          if (string.attrib['CONTENT'][0].isupper()) & (string.attrib['CONTENT'][-1] in ['.',',']) & (string.attrib['CONTENT'].upper() not in removelist):
            # print(string.attrib['CONTENT'])
            motherword_list.append(string.attrib['CONTENT'].upper())
          if (string.attrib['CONTENT'][0].isupper()) & (string.attrib['CONTENT'][-1] == '-') & (string.attrib['CONTENT'].upper() not in removelist):
            motherword_list.append(string.attrib['CONTENT'].upper())
            hyphen_signal = True
            continue
          if hyphen_signal == True:
            motherword_list.append(string.attrib['CONTENT'].upper())
            hyphen_signal = False
            continue
          if string.attrib['CONTENT'] == 'fig.':
            # print('Figure Signal - ', string.attrib['CONTENT'])
            motherword_list.append(string.attrib['CONTENT'])
          see_signal = False
          hyphen_signal = False
      # print(len(wordlist))
      # print(wordlist)

      for sublist in wordlist:
        if ('See' in sublist) & ('fig.' not in sublist) & (len(sublist) in range(2,6)):
          if (sublist[-1] not in ['See','or']) & (sublist.count('See') == 1):
            word_relate_list.append(sublist)
      print(word_relate_list)
      relate_count += len(word_relate_list)
print('relate = ', relate_count)
