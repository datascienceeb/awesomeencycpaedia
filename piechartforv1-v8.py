# -*- coding: utf-8 -*-
"""
Created on Sun Nov 24 10:39:28 2019

@author: xue feng
"""

import seaborn as sns
import pandas
import matplotlib.pyplot as plt
from palettable.colorbrewer.qualitative import Pastel1_7
#colour package

#natural language processing package
from pyecharts.charts import Bar
from pyecharts import options as opts
#import necessary package

v1_data = pandas.read_csv('V1.csv', low_memory=False)
v2_data = pandas.read_csv('V2.csv', low_memory=False)
v3_data = pandas.read_csv('V3.csv', low_memory=False)
v4_data = pandas.read_csv('V4.csv', low_memory=False)
v5_data = pandas.read_csv('V5.csv', low_memory=False)
v6_data = pandas.read_csv('V6.csv', low_memory=False)
v7_data = pandas.read_csv('V7.csv', low_memory=False)
v8_data = pandas.read_csv('V8.csv', low_memory=False)
initialcharacters='A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
#set a array with 26 alphabet

v1_data['Initial_1']=''
v2_data['Initial_2']=''
v3_data['Initial_3']=''
v4_data['Initial_4']=''
v5_data['Initial_5']=''
v6_data['Initial_6']=''
v7_data['Initial_7']=''
v8_data['Initial_8']=''

for i in v1_data.index:
    v1_data.loc[i]['Initial_1'] = v1_data.loc[i]['CONTENT'][0]
for i in v2_data.index:
    v2_data.loc[i]['Initial_2'] = v2_data.loc[i]['CONTENT'][0]
for i in v3_data.index:
    v3_data.loc[i]['Initial_3'] = v3_data.loc[i]['CONTENT'][0]    
for i in v4_data.index:
    v4_data.loc[i]['Initial_4'] = v4_data.loc[i]['CONTENT'][0]
for i in v5_data.index:
    v5_data.loc[i]['Initial_5'] = v5_data.loc[i]['CONTENT'][0]
for i in v6_data.index:
    v6_data.loc[i]['Initial_6'] = v6_data.loc[i]['CONTENT'][0]
for i in v7_data.index:
    v7_data.loc[i]['Initial_7'] = v7_data.loc[i]['CONTENT'][0]    
for i in v8_data.index:
    v8_data.loc[i]['Initial_8'] = v8_data.loc[i]['CONTENT'][0]



white_circle=plt.Circle( (0,0),1, color='white')
#create white circle for the center
plt.figure(figsize=[10,5],dpi=60)
#set the size of figure
plt.pie(v1_data['Initial_1'].value_counts(), labels=initialcharacters,radius=3,labeldistance = 1.1, colors=Pastel1_7.hex_colors,autopct='%1.1f%%',pctdistance = 0.9)
#adjusti from 1 to 8
p=plt.gcf()
p.gca().add_artist(white_circle)
plt.legend(loc='best', bbox_to_anchor=(1.4, 0.4, 0.5, 0.5))
plt.show()


