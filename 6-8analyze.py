# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 20:46:53 2019

@author: 灵
"""

#!pip install palettable
import seaborn as sns
import re
import pandas as pd
import matplotlib.pyplot as plt
from palettable.colorbrewer.qualitative import Pastel1_7

#import numpy as np
#from operator import itemgetter
try:
  import xml.etree.cElementTree as et
except ImportError:
  import xml.etree.ElementTree as et
import glob
editionNumberSixth = [192547781, 192015835, 193696080, 192200662, 192692756, 192200322, 192015838, 192015836, 192547782, 191679036, 192984257, 191689064, 192200323, 191689065, 192015837, 192547783, 192547784, 192015834, 193057497, 193696081]
editionNumberSixthSupplement = [193057498, 193108319, 192016360, 192146221, 192547786, 192547787, 193108320, 192693395, 193057499, 193696082, 192547788, 192693396, 192547790, 193109112, 193108321]
#Exclude the Index
editionNumberSeventh = [192984259, 193057500, 193108322, 193696083, 193322690, 193819043, 193322688, 193696084, 193469090, 193638940, 192693199, 193108323, 193322689, 193819044, 194474782, 193469091, 193469092, 193057501, 193913444, 193819045]
#Exclude the Index
editionNumberEighth = [193322698, 193696085, 193696086, 193108324, 193109113, 193109114, 193108325, 193322700, 193109115, 193469392, 193696087, 193916150, 193696088, 193592632, 193322699, 193819046, 193108326, 193322701, 193469393, 193819047]

version2words=[]
#create an empty list to save the data from xml file.
version6FileSum=[]
#for version 4 and 5 ,the volumns are too much ,so I used for loop to save all the files in versionfilesum this list.
for a in editionNumberSixth:
    version6File1=glob.glob(str(a)+"/alto/*.xml")
    for b in version6File1:
        version6FileSum.append(b)
#save all xml filename in the file.
for filename in version6FileSum:
    volumn=0
    tree2 = et.parse(filename)
    root1 = tree2.getroot()
    attr = root1.attrib
    tagtext = '{http://www.loc.gov/standards/alto/v3/alto.xsd}'
    path = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine/'+tagtext+'String'
    pathforno=tagtext+'Description/'+tagtext+'sourceImageInformation/'+tagtext+'String'
    content=0
    newcontent=0
    if (re.match("^192547781[0-9\.a-z\\\/]{0,40}$",filename)):
        volumn=1
    elif re.match("^192015835[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=2
    elif re.match("^193696080[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=3
    elif re.match("^192200662[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=4
    elif re.match("^192692756[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=5
    elif re.match("^192200322[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=6
    elif re.match("^192015838[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=7
    elif re.match("^192015836[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=8
    elif re.match("^192547782[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=9
    elif re.match("^191679036[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=10
    elif re.match("^192984257[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=11
    elif re.match("^191689064[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=12
    elif re.match("^192200323[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=13
    elif re.match("^191689065[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=14
    elif re.match("^192015837[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=15
    elif re.match("^192547783[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=16
    elif re.match("^192547784[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=17
    elif re.match("^192015834[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=18
    elif re.match("^193057497[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=19
    elif re.match("^193696081[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=20
    for a in tree2.iterfind(path):
        content = a.attrib['CONTENT']#Get the text content
        if ((content.isupper()) & (len(content)>1)):        
            if re.match("^[A-Z]([A-Za-z]{0,40})*\,$",a.attrib['CONTENT']):
                #match the word which has a "," at the end 
                version2words.append({'CONTENT':str(a.attrib['CONTENT'].strip(',')),'PAGE':str(re.findall(r"p\d{0,3}",a.attrib['ID'])).strip('[]\''),'BOOKNUM':volumn})
    dfversion6 = pd.DataFrame(version2words,columns=["CONTENT","PAGE","BOOKNUM"])
dfversion6.sort_values(by=['CONTENT'])



 
dfversion6 = dfversion6.drop_duplicates(subset=['CONTENT'], keep='first', inplace=False).reset_index(drop=True)


dfversion6


#Data cleaning for version 4
version8=pd.DataFrame(columns=["CONTENT","PAGE","BOOKNUM"])
for m in range(ord("A"),ord("Z")+1): 
        versiontemp=dfversion6[dfversion6.CONTENT.str.startswith(chr(m))]
        if m == 65:#A
                Dlt=versiontemp[(versiontemp['BOOKNUM']>3)].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                #print(version1)
        elif m==66:#B
                Dlt=versiontemp[((versiontemp['BOOKNUM']<3)|(versiontemp['BOOKNUM']>5))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==67:#C
                Dlt=versiontemp[((versiontemp['BOOKNUM']<5)|(versiontemp['BOOKNUM']>7))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==68 :#D
                Dlt=versiontemp[((versiontemp['BOOKNUM']<7)|(versiontemp['BOOKNUM']>7))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==69:#E
                Dlt=versiontemp[((versiontemp['BOOKNUM']<7)|(versiontemp['BOOKNUM']>8))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==70:#F
                Dlt=versiontemp[((versiontemp['BOOKNUM']<8)|(versiontemp['BOOKNUM']>9))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==71:#G
                Dlt=versiontemp[((versiontemp['BOOKNUM']<9)|(versiontemp['BOOKNUM']>10))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp  
        elif m==72:#H
                Dlt=versiontemp[((versiontemp['BOOKNUM']<10)|(versiontemp['BOOKNUM']>11))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                
        elif ((m>=73) & (m<=75)):#IJK
                Dlt=versiontemp[((versiontemp['BOOKNUM']<11)|(versiontemp['BOOKNUM']>11))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==76:#L
                Dlt=versiontemp[((versiontemp['BOOKNUM']<11)|(versiontemp['BOOKNUM']>12))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==77:#M
                Dlt=versiontemp[((versiontemp['BOOKNUM']<12)|(versiontemp['BOOKNUM']>14))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp 
        elif m==78:#N
                Dlt=versiontemp[((versiontemp['BOOKNUM']<14)|(versiontemp['BOOKNUM']>15))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                 
        elif m==79:#O
                Dlt=versiontemp[((versiontemp['BOOKNUM']<15)|(versiontemp['BOOKNUM']>15))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp                 
        elif m==80:#P
                Dlt=versiontemp[((versiontemp['BOOKNUM']<15)|(versiontemp['BOOKNUM']>17))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==80:#Q
                Dlt=versiontemp[((versiontemp['BOOKNUM']<17)|(versiontemp['BOOKNUM']>17))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp  
        elif m==81:#R
                Dlt=versiontemp[((versiontemp['BOOKNUM']<17)|(versiontemp['BOOKNUM']>18))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==82:#S
                Dlt=versiontemp[((versiontemp['BOOKNUM']<18))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif ((m>=83) and (m<95)):#T-Z
                Dlt=versiontemp[((versiontemp['BOOKNUM']<20))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        version6=version6.append(version1,ignore_index=True)

version6#get the version 4 dataframe





version2words=[]
#create an empty list to save the data from xml file.
version7FileSum=[]
#for version 4 and 5 ,the volumns are too much ,so I used for loop to save all the files in versionfilesum this list.
for a in editionNumberSeventh:
    version7File1=glob.glob(str(a)+"/alto/*.xml")
    for b in version7File1:
        version7FileSum.append(b)
#save all xml filename in the file.
for filename in version7FileSum:
    volumn=0
    tree2 = et.parse(filename)
    root1 = tree2.getroot()
    attr = root1.attrib
    tagtext = '{http://www.loc.gov/standards/alto/v3/alto.xsd}'
    path = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine/'+tagtext+'String'
    pathforno=tagtext+'Description/'+tagtext+'sourceImageInformation/'+tagtext+'String'
    content=0
    newcontent=0
    if (re.match("^192984259[0-9\.a-z\\\/]{0,40}$",filename)):
        volumn=1
    elif re.match("^193057500[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=2
    elif re.match("^193108322[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=3
    elif re.match("^193696083[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=4
    elif re.match("^193322690[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=5
    elif re.match("^193819043[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=6
    elif re.match("^193322688[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=7
    elif re.match("^193696084[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=8
    elif re.match("^193469090[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=9
    elif re.match("^193638940[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=10
    elif re.match("^192693199[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=11
    elif re.match("^193108323[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=12
    elif re.match("^193322689[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=13
    elif re.match("^193819044[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=14
    elif re.match("^194474782[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=15
    elif re.match("^193469091[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=16
    elif re.match("^193469092[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=17
    elif re.match("^193057501[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=18
    elif re.match("^193913444[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=19
    elif re.match("^193819045[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=20
    for a in tree2.iterfind(path):
        content = a.attrib['CONTENT']#Get the text content
        if ((content.isupper()) & (len(content)>1)):        
            if re.match("^[A-Z]([A-Za-z]{0,40})*\,$",a.attrib['CONTENT']):
                #match the word which has a "," at the end 
                version2words.append({'CONTENT':str(a.attrib['CONTENT'].strip(',')),'PAGE':str(re.findall(r"p\d{0,3}",a.attrib['ID'])).strip('[]\''),'BOOKNUM':volumn})
    dfversion7 = pd.DataFrame(version2words,columns=["CONTENT","PAGE","BOOKNUM"])
dfversion7.sort_values(by=['CONTENT'])
dfversion7 = dfversion7.drop_duplicates(subset=['CONTENT'], keep='first', inplace=False).reset_index(drop=True)
dfversion7





#Data cleaning for version 4
version7=pd.DataFrame(columns=["CONTENT","PAGE","BOOKNUM"])
for m in range(ord("A"),ord("Z")+1): 
        versiontemp=dfversion7[dfversion7.CONTENT.str.startswith(chr(m))]
        if m == 65:#A
                Dlt=versiontemp[(versiontemp['BOOKNUM']>3)].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                #print(version1)
        elif m==66:#B
                Dlt=versiontemp[((versiontemp['BOOKNUM']<3)|(versiontemp['BOOKNUM']>4))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==67:#C
                Dlt=versiontemp[((versiontemp['BOOKNUM']<4)|(versiontemp['BOOKNUM']>6))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==68 :#D
                Dlt=versiontemp[((versiontemp['BOOKNUM']<6)|(versiontemp['BOOKNUM']>7))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==69:#E
                Dlt=versiontemp[((versiontemp['BOOKNUM']<7)|(versiontemp['BOOKNUM']>8))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==70:#F
                Dlt=versiontemp[((versiontemp['BOOKNUM']<8)|(versiontemp['BOOKNUM']>9))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==71:#G
                Dlt=versiontemp[((versiontemp['BOOKNUM']<9)|(versiontemp['BOOKNUM']>10))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp  
        elif m==72:#H
                Dlt=versiontemp[((versiontemp['BOOKNUM']<10)|(versiontemp['BOOKNUM']>11))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                
        elif ((m>=73) & (m<=75)):#IJK
                Dlt=versiontemp[((versiontemp['BOOKNUM']<11)|(versiontemp['BOOKNUM']>11))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==76:#L
                Dlt=versiontemp[((versiontemp['BOOKNUM']<12)|(versiontemp['BOOKNUM']>12))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==77:#M
                Dlt=versiontemp[((versiontemp['BOOKNUM']<12)|(versiontemp['BOOKNUM']>14))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp 
        elif m==78:#N
                Dlt=versiontemp[((versiontemp['BOOKNUM']<14)|(versiontemp['BOOKNUM']>15))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                 
        elif m==79:#O
                Dlt=versiontemp[((versiontemp['BOOKNUM']<15)|(versiontemp['BOOKNUM']>15))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp                 
        elif m==80:#P
                Dlt=versiontemp[((versiontemp['BOOKNUM']<15)|(versiontemp['BOOKNUM']>17))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==80:#Q
                Dlt=versiontemp[((versiontemp['BOOKNUM']<17)|(versiontemp['BOOKNUM']>17))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp  
        elif m==81:#R
                Dlt=versiontemp[((versiontemp['BOOKNUM']<18)|(versiontemp['BOOKNUM']>18))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==82:#S
                Dlt=versiontemp[((versiontemp['BOOKNUM']<18))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif ((m>=83) and (m<95)):#T-Z
                Dlt=versiontemp[((versiontemp['BOOKNUM']<20))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        version7=version7.append(version1,ignore_index=True)

version7#get the version 7 dataframe





version8FileSum=[]
#for version 4 and 5 ,the volumns are too much ,so I used for loop to save all the files in versionfilesum this list.
for a in editionNumberEighth:
    version8File1=glob.glob(str(a)+"/alto/*.xml")
    for b in version8File1:
        version8FileSum.append(b)
#save all xml filename in the file.
for filename in version8FileSum:
    volumn=0
    tree2 = et.parse(filename)
    root1 = tree2.getroot()
    attr = root1.attrib
    tagtext = '{http://www.loc.gov/standards/alto/v3/alto.xsd}'
    path = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine/'+tagtext+'String'
    pathforno=tagtext+'Description/'+tagtext+'sourceImageInformation/'+tagtext+'String'
    content=0
    newcontent=0
    if (re.match("^193322698[0-9\.a-z\\\/]{0,40}$",filename)):
        volumn=1
    elif re.match("^193696085[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=2
    elif re.match("^193696086[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=3
    elif re.match("^193108324[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=4
    elif re.match("^193109113[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=5
    elif re.match("^193109114[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=6
    elif re.match("^193108325[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=7
    elif re.match("^193322700[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=8
    elif re.match("^193109115[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=9
    elif re.match("^193469392[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=10
    elif re.match("^193696087[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=11
    elif re.match("^193916150[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=12
    elif re.match("^193696088[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=13
    elif re.match("^193592632[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=14
    elif re.match("^193322699[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=15
    elif re.match("^193819046[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=16
    elif re.match("^193108326[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=17
    elif re.match("^193322701[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=18
    elif re.match("^193469393[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=19
    elif re.match("^193819047[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=20
    for a in tree2.iterfind(path):
        content = a.attrib['CONTENT']#Get the text content
        if ((content.isupper()) & (len(content)>1)):        
            if re.match("^[A-Z]([A-Za-z]{0,40})*\,$",a.attrib['CONTENT']):
                #match the word which has a "," at the end 
                version2words.append({'CONTENT':str(a.attrib['CONTENT'].strip(',')),'PAGE':str(re.findall(r"p\d{0,3}",a.attrib['ID'])).strip('[]\''),'BOOKNUM':volumn})
    dfversion8 = pd.DataFrame(version2words,columns=["CONTENT","PAGE","BOOKNUM"])
dfversion8.sort_values(by=['CONTENT'])



 
dfversion8 = dfversion8.drop_duplicates(subset=['CONTENT'], keep='first', inplace=False).reset_index(drop=True)


dfversion8

version8=pd.DataFrame(columns=["CONTENT","PAGE","BOOKNUM"])
for m in range(ord("A"),ord("Z")+1): 
        versiontemp=dfversion8[dfversion8.CONTENT.str.startswith(chr(m))]
        if m == 65:#A
                Dlt=versiontemp[(versiontemp['BOOKNUM']>3)].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                #print(version1)
        elif m==66:#B
                Dlt=versiontemp[((versiontemp['BOOKNUM']<3)|(versiontemp['BOOKNUM']>4))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==67:#C
                Dlt=versiontemp[((versiontemp['BOOKNUM']<4)|(versiontemp['BOOKNUM']>6))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==68 :#D
                Dlt=versiontemp[((versiontemp['BOOKNUM']<6)|(versiontemp['BOOKNUM']>7))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==69:#E
                Dlt=versiontemp[((versiontemp['BOOKNUM']<7)|(versiontemp['BOOKNUM']>8))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==70:#F
                Dlt=versiontemp[((versiontemp['BOOKNUM']<8)|(versiontemp['BOOKNUM']>9))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==71:#G
                Dlt=versiontemp[((versiontemp['BOOKNUM']<9)|(versiontemp['BOOKNUM']>10))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp  
        elif m==72:#H
                Dlt=versiontemp[((versiontemp['BOOKNUM']<10)|(versiontemp['BOOKNUM']>11))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                
        elif ((m>=73) & (m<=75)):#IJK
                Dlt=versiontemp[((versiontemp['BOOKNUM']<11)|(versiontemp['BOOKNUM']>11))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==76:#L
                Dlt=versiontemp[((versiontemp['BOOKNUM']<12)|(versiontemp['BOOKNUM']>12))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==77:#M
                Dlt=versiontemp[((versiontemp['BOOKNUM']<12)|(versiontemp['BOOKNUM']>14))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp 
        elif m==78:#N
                Dlt=versiontemp[((versiontemp['BOOKNUM']<14)|(versiontemp['BOOKNUM']>15))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
                 
        elif m==79:#O
                Dlt=versiontemp[((versiontemp['BOOKNUM']<15)|(versiontemp['BOOKNUM']>15))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp                 
        elif m==80:#P
                Dlt=versiontemp[((versiontemp['BOOKNUM']<15)|(versiontemp['BOOKNUM']>17))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==80:#Q
                Dlt=versiontemp[((versiontemp['BOOKNUM']<17)|(versiontemp['BOOKNUM']>17))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp  
        elif m==81:#R
                Dlt=versiontemp[((versiontemp['BOOKNUM']<18)|(versiontemp['BOOKNUM']>18))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif m==82:#S
                Dlt=versiontemp[((versiontemp['BOOKNUM']<18))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        elif ((m>=83) and (m<95)):#T-Z
                Dlt=versiontemp[((versiontemp['BOOKNUM']<20))].index
                versiontemp.drop(Dlt,inplace=True)
                version1=versiontemp
        version8=version8.append(version1,ignore_index=True)

version8#get the version 7 dataframe