import pandas as pd
import numpy as np
from lxml import etree

with open('encyclopaediaBritannica/144133901/144133901-mets.xml','r') as schema_file:
    doc = etree.parse(schema_file)
print(doc)
root = doc.getroot()
print(root)
list = []
for child in root.iter():
    a=dict(child.attrib)
    # if len(a.keys()) == 4:
        # list.append(a)
    if (a.__contains__('LABEL')&a.__contains__('TYPE')) or a.__contains__('FILEID'):
        list.append(a)

df = pd.DataFrame(list)

nan1 = df.iloc[1,1]

# print(df.head(30))
df1 = df[(df['ID'].notna()) | (df['FILEID'].str.contains(r'\.34', na=False))]
# print(df1.head(10))
newindex = []
index1 = []
indexcolumns = ['ID', 'LABEL', 'TYPE', 'ORDER']
for index, row in df1.iterrows():
    if str(row['ID']) != 'nan':
        row0 = row[indexcolumns]
        index1 = row.tolist()

    else:
        lis = row0.to_list()
        lis.append(row['FILEID'])
        newindex.append(lis)
    # print(newindex)
#print(newindex)
df2 = pd.DataFrame(newindex,columns=['ID', 'LABEL', 'TYPE', 'ORDER','FILEID']).set_index(indexcolumns)
print(df2.tail(50))
