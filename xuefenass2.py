# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 18:27:59 2019

@author: xue feng
"""

import re
import pandas as pd
#import numpy as np
#from operator import itemgetter
try:
  import xml.etree.cElementTree as et
except ImportError:
  import xml.etree.ElementTree as et
import glob
version1words=[]

version1File=glob.glob(r"144133901-1/alto/*.xml")+glob.glob(r"144133902-1/alto/*.xml")+glob.glob(r"144133903-1/alto/*.xml")
for filename in version1File:
    volumn=0
    tree2 = et.parse(filename)
    root1 = tree2.getroot()
    attr = root1.attrib
    tagtext = '{http://www.loc.gov/standards/alto/v3/alto.xsd}'
    path = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine/'+tagtext+'String'
    path1 = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine'
    pathforno=tagtext+'Description/'+tagtext+'sourceImageInformation/'+tagtext+'String'
    content=0
    newcontent=0
    if re.match("^144133901-1*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=1
    elif re.match("^144133902-1*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=2
    elif re.match("^144133903-1*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=3
    for textline in tree2.iterfind(path1):
        list1 = []
        for string in textline.iterfind(tagtext+'String'):
            list1.append(string.attrib['CONTENT'])
            if len(list1) == 1 :
                if re.match("^[A-Z]([A-Z]{0,40})*\.$",string.attrib['CONTENT']):
                    print(list1)
    for a in tree2.iterfind(path):
        content = a.attrib['CONTENT']#Get the text content
        if ((content.isupper()) & (len(content)>1)):        
            if re.match("^[A-Z]([A-Za-z]{0,40})*\,$",a.attrib['CONTENT']):
                #match the word which has a "," at the end 
                version1words.append({'CONTENT':str(a.attrib['CONTENT'].strip(',')),'PAGE':str(re.findall(r"p\d{0,3}",a.attrib['ID'])).strip('[]\''),'BOOKNUM':volumn})
    dfversion1 = pd.DataFrame(version1words,columns=["CONTENT","PAGE","BOOKNUM"])

dfversion1.sort_values(by=['CONTENT']) 
dfversion1 = dfversion1.drop_duplicates(subset=['CONTENT'], keep='first', inplace=False).reset_index(drop=True)
dfversion1




version1_1771=pd.DataFrame(columns=["CONTENT","PAGE","BOOKNUM"])
for m in range(ord("A"),ord("Z")+1):
    
    versiontemp=dfversion1[dfversion1.CONTENT.str.startswith(chr(m))]
    if  m<=66:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==2)|(versiontemp['BOOKNUM']==3)].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
        print(version1)
    elif  (m>66) & (m<=76):
        Dlt=versiontemp[(versiontemp['BOOKNUM']==1)|(versiontemp['BOOKNUM']==3)].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif  (m>76) & (m<=95):
        Dlt=versiontemp[(versiontemp['BOOKNUM']==1)|(versiontemp['BOOKNUM']==2)].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    version1_1771=version1_1771.append(version1,ignore_index=True)
print(version1_1771.head(2000))

version2words=[]
version2File=glob.glob(r"144850370-3/alto/*.xml")+glob.glob(r"144850373-3/alto/*.xml")+glob.glob(r"144850374-3/alto/*.xml")+glob.glob(r"144850375-3/alto/*.xml")+glob.glob(r"144850376-3/alto/*.xml")+glob.glob(r"144850377-3/alto/*.xml")+glob.glob(r"144850378-3/alto/*.xml")+glob.glob(r"144850379-3/alto/*.xml")+glob.glob(r"190273289-3/alto/*.xml")+glob.glob(r"190273290-3/alto/*.xml")
for filename in version2File:
    volumn=0
    tree2 = et.parse(filename)
    root1 = tree2.getroot()
    attr = root1.attrib
    tagtext = '{http://www.loc.gov/standards/alto/v3/alto.xsd}'
    path = tagtext+'Layout/'+tagtext+'Page/'+tagtext+'PrintSpace/'+tagtext+'TextLine/'+tagtext+'String'
    pathforno=tagtext+'Description/'+tagtext+'sourceImageInformation/'+tagtext+'String'
    content=0
    newcontent=0
    if re.match("^144850370-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=1
    elif re.match("^144850373-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=2
    elif re.match("^144850374-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=3
    elif re.match("^144850375-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=4
    elif re.match("^144850376-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=5
    elif re.match("^144850377-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=6
    elif re.match("^144850378-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=7
    elif re.match("^144850379-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=8
    elif re.match("^190273289-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=9
    elif re.match("^190273290-3*[0-9\.a-z\\\/]{0,40}$",filename):
        volumn=10
    for a in tree2.iterfind(path):
        content = a.attrib['CONTENT']#Get the text content
        if ((content.isupper()) & (len(content)>1)):        
            if re.match("^[A-Z]([A-Za-z]{0,40})*\,$",a.attrib['CONTENT']):
                #match the word which has a "," at the end 
                version2words.append({'CONTENT':str(a.attrib['CONTENT'].strip(',')),'PAGE':str(re.findall(r"p\d{0,3}",a.attrib['ID'])).strip('[]\''),'BOOKNUM':volumn})
                
    dfversion2 = pd.DataFrame(version2words,columns=["CONTENT","PAGE","BOOKNUM"])

dfversion2.sort_values(by=['CONTENT']) 
dfversion2 = dfversion2.drop_duplicates(subset=['CONTENT'], keep='first', inplace=False).reset_index(drop=True)
dfversion2


version2=pd.DataFrame(columns=["CONTENT","PAGE","BOOKNUM"])
for m in range(ord("A"),ord("Z")+1):
    versiontemp=dfversion1[dfversion1.CONTENT.str.startswith(chr(m))]
    if  m<=66:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(3,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
        
    elif m==67:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(4,11))|(versiontemp['BOOKNUM']==range(1,3))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
        print(version1)
    elif m>=68 & m<=70:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,4))|(versiontemp['BOOKNUM']==range(5,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m>=71 & m<=74:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,5))|(versiontemp['BOOKNUM']==range(6,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m>=75 & m<=76:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,6))|(versiontemp['BOOKNUM']==range(7,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m==77:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,6))|(versiontemp['BOOKNUM']==range(8,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m==78:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,7))|(versiontemp['BOOKNUM']==range(9,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m==79:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,7))|(versiontemp['BOOKNUM']==range(8,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m==80:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,8))|(versiontemp['BOOKNUM']==range(10,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m>=81 & m<=82:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,9))|(versiontemp['BOOKNUM']==range(10,11))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    elif m>=83 & m<=95:
        Dlt=versiontemp[(versiontemp['BOOKNUM']==range(1,10))].index
        versiontemp.drop(Dlt,inplace=True)
        version1=versiontemp
    version2=version2.append(version1,ignore_index=True)
print(version2)